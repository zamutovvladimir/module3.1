﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Module_3
{
    class Program
    {
        protected Program()
        {
        }

        static void Main(string[] args)
        {
            ///// Use this method to implement tasks

            #region Task 1 // Проверка первого задания.
            Task1 firstTask = new Task1();
            Console.WriteLine("Task 1: {0}", firstTask.Multiplication(-5, 5));
            Console.WriteLine("Task 1: {0}", firstTask.Multiplication(5, -5));
            Console.WriteLine("Task 1: {0}", firstTask.Multiplication(-5, -5));
            Console.WriteLine("Task 1: {0}", firstTask.Multiplication(5, 5));
            Console.WriteLine("Task 1: {0}", firstTask.Multiplication(0, 5));
            Console.WriteLine("Task 1: {0}", firstTask.Multiplication(5, 0));
            Console.WriteLine("Task 1: {0}", firstTask.Multiplication(0, 0));

            Console.WriteLine(firstTask.ParseAndValidateIntegerNumber("-5"));
            Console.WriteLine(firstTask.ParseAndValidateIntegerNumber("5"));
            try
            {
                Console.WriteLine(firstTask.ParseAndValidateIntegerNumber("-3.5"));
            }
            catch (ArgumentException e)
            {
                Console.WriteLine("Метод завершился исключением!\nИнформация об исключении: {0}", e.Message);
            }
            #endregion

            #region Task 2 // Проверка второго задания.
            int digit;
            List<int> list;
            Task2 secondTask = new Task2();


            Console.Write("Введите натуральное число: ");
            secondTask.TryParseNaturalNumber(Console.ReadLine(), out digit);

            list = secondTask.GetEvenNumbers(digit);

            foreach (var item in list)
            {
                Console.WriteLine("{0}\t", item);
            }
            #endregion

            #region Task 3 // Проверка третьего задания.

            Task3 thirdTask = new Task3();
            int source, digitToRemove;
            Console.Write("Введите натуральное число: ");
            thirdTask.TryParseNaturalNumber(Console.ReadLine(), out source);
            Console.Write("Введите цифру: ");
            thirdTask.TryParseNaturalNumber(Console.ReadLine(), out digitToRemove);
            Console.WriteLine("Результат удаления: {0}",
                thirdTask.RemoveDigitFromNumber(source, digitToRemove));

            #endregion

            // Просмотр результата в консоли.
            Console.ReadLine();
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (int.TryParse(source, out int result))
            {
                return result;
            }
            throw new ArgumentException("Необходимо ввести натуральное число!");
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;
            bool negativResult = false;

            if ((num1 < 0) ^ (num2 < 0))
                negativResult = true;

            num1 = Math.Abs(num1);
            num2 = Math.Abs(num2);
            
            if (negativResult)
            {
                
                for (int i = 0; i < num2; i++)
                    result -= num1;
            }
            else
            {
                for (int i = 0; i < num2; i++)
                    result += num1;
            }
            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool secondAttempt = false;
            while ((!int.TryParse(input, out result)) || (result < 0))
            {
                if (secondAttempt)
                    return false;
                
                secondAttempt = true;
                Console.Write("Введено некоректное значение! Повторите ввод: ");                
                input = Console.ReadLine();
            }

            return true;
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> list = new List<int>();
            for (int i = 0; i <= naturalNumber; i += 2)
            {
                list.Add(i);
            }
            return list;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            Task2 secondTask = new Task2();
            return secondTask.TryParseNaturalNumber(input, out result);
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string inputString = Convert.ToString(source);
            System.Text.StringBuilder outputString = new System.Text.StringBuilder();
            string digitString = Convert.ToString(digitToRemove);

            for (int i = 0; i < inputString.Length; i++)
            {
                if (!inputString[i].ToString().Equals(digitString))
                {
                    
                    outputString.Append(inputString[i]);
                }
            }

            return outputString.ToString();
        }
    }
}
